import setuptools

test_requirements = []

setuptools.setup(
    name='timeseries-lib',
    version="0.1",
    packages=setuptools.find_packages(),
    description='',
    author='Alexander Schulz',
    author_email='info@alexander-schulz.eu',
    keywords=['pip', 'timeseries', 'pandas', 'units', 'time_zone', 'alignment'],
    url="",
    classifiers=[
        "Operating System :: OS Independent",
    ],
    install_requires=[
        'dateutil>=2.7.3',
        'numpy>=1.16.6',
        'pandas>=0.24.2',
        'pint>=0.9',
        'pytz>=2016.4',
        'six>=1.14.0',
    ],
    python_requires='>=2.7',
    test_suite='nose.collector',
    tests_require=test_requirements,
    extras_require={
        "test": test_requirements
    }
)
