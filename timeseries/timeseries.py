import collections
import copy
import datetime as dt
import numbers

import dateutil
import numpy as np
import pandas as pd
import pytz

from .constants.align import Alignment, Alignments, alignments
from .constants.freq import Frequency, frequencies, CustomFrequency
from .constants.unit import Unit, units


class Timeseries(object):
    """
    Attributes:
        _series (pandas.Series):
        _unit (Unit):
        _freq (Frequency):
        _align (Alignment):
        _time_zone (str):
    """
    @staticmethod
    def create_null_timeseries(start, end, freq, unit=None, align='left', time_zone='infer'):
        return Timeseries.create_constant_timeseries(start, end, np.NaN, unit, freq, align=align, time_zone=time_zone)

    @staticmethod
    def create_constant_timeseries(start, end, value, freq, unit=None, align='left', time_zone='infer'):
        inferred_tz = Timeseries._infer_tz_from_timestamp(start)
        index = pd.date_range(start, end, freq=freq.pd_timedelta, tz=inferred_tz, closed='left')
        series = pd.Series([value] * len(index), index=index)
        return Timeseries.create_from_pd_series(series, freq=freq, unit=unit, align=align, time_zone=time_zone)

    @staticmethod
    def create_from_lists(timestamps, values, freq='infer', unit=None,  align='left', time_zone='infer'):
        tuples = list(zip(timestamps, values))
        return Timeseries.create_from_tuples(tuples, freq=freq, unit=unit, align=align, time_zone=time_zone)

    @staticmethod
    def create_from_tuples(tuples, freq='infer', unit=None, align='left', time_zone='infer'):
        dictionary = {k: v for k, v in tuples}
        return Timeseries.create_from_dict(dictionary, freq=freq, unit=unit, align=align, time_zone=time_zone)

    @staticmethod
    def create_from_dict(dictionary, freq='infer', unit=None, align='left', time_zone='infer'):
        series = pd.Series(dictionary)
        return Timeseries.create_from_pd_series(series, freq=freq, unit=unit, align=align, time_zone=time_zone)

    @staticmethod
    def create_from_pd_series(series, freq='infer', unit=None, align='left', time_zone='infer'):
        return Timeseries(series, align, freq=freq, unit=unit, time_zone=time_zone)

    def __init__(self, series, align, freq=None, unit=None, time_zone=None, sort=True):
        """
        Args:
            series (pandas.Series): a pandas series that holds timestamps (pandas.DatetimeIndex) and numeric values
                (may have own freq and own timezone, that need to match the other params)
            align (Alignment or str): the alignment of timestamps of the series, Alignment-object or 'left' or 'right',
                default: None
            freq (Frequency or str, optional): the frequency of the series, Frequency-object or None or 'infer',
                default: None
            unit (Unit or str, optional): the measurement unit of the series,  Unit-object or None, default: None
            time_zone (str, optional): time zone name (https://en.wikipedia.org/wiki/List_of_tz_database_time_zones)
                or None or 'infer', default: None
            sort (bool, optional): sort the series in ascending order, default: True
        """
        self._series = self._validate_series(series)
        if sort:
            self._series = self._series.sort_index(ascending=True)
        self._align = self._validate_align(align)
        self._unit = self._validate_unit(unit)
        self._freq = self._validate_freq(freq)
        if self._freq is not None:
            # apply freq to series
            self._series.index.freq = self._freq.pd_timedelta
        self._time_zone = self._validate_time_zone(time_zone)
        if self._time_zone is not None:
            # apply time zone to series (and eliminate dateutil tzinfos, because tz_convert uses pytz)
            self._series = self._series.tz_convert(time_zone)

    @property
    def unit(self):
        return self._unit

    @property
    def freq(self):
        return self._freq

    @property
    def align(self):
        return self._align

    @property
    def time_zone(self):
        return self._time_zone

    @property
    def start_timestamp(self):
        if not self.empty:
            return self._series.index[0].to_pydatetime()
        else:
            raise ValueError('Empty Timeseries')

    @property
    def end_timestamp(self):
        if not self.empty:
            return self._series.index[-1].to_pydatetime()
        else:
            raise ValueError('Empty Timeseries')

    @property
    def time_range(self):
        if not self.empty:
                return self.start_timestamp, self.end_timestamp
        else:
            raise ValueError('Empty Timeseries')

    @property
    def first(self):
        if not self.empty:
            return self._series.index[0], self._series.values[0]
        else:
            raise ValueError('Empty Timeseries')

    @property
    def last(self):
        if not self.empty:
            return self._series.index[-1], self._series.values[-1]
        else:
            raise ValueError('Empty Timeseries')

    @property
    def empty(self):
        return self._series.empty

    @property
    def values(self):
        return self._series.values.tolist()

    @property
    def timestamps(self):
        return self._series.index.to_pydatetime().tolist()

    def set_time_zone(self, tz):
        if self._time_zone is not None:
            raise TypeError('Time zone is already set to {}. Use to_time_zone for conversion'.format(self._time_zone))
        self._series.index = self._series.index.tz_localize(tz)
        self._time_zone = tz
        return self

    def naive_tz(self):
        return self._time_zone(None)

    def to_time_zone(self, tz):
        if self._time_zone is None:
            raise TypeError('Time zone is naive. Cannot convert naive time zone. Use set_time_zone')
        self._series.index = self._series.index.tz_convert(tz)
        self._time_zone = tz
        return self

    def to_freq(self, freq):
        if self.freq is None:
            raise ValueError('cannot convert a series with freq=None')
        if self._series.index.freq < pd.Timedelta('0'):
            raise NotImplementedError('frequency conversion is not implemented for descending timestamps')
        # TODO : handle with unit
        factor = abs(freq.pd_timedelta.total_seconds() / self.freq.pd_timedelta.total_seconds())
        new_ts = copy.deepcopy(self)
        new_ts = new_ts.to_left_align()
        new_ts._series = self._series.append(
            self._series.tshift(freq=self.freq.pd_timedelta).iloc[-1:]).resample(
            rule=freq.pd_timedelta).mean().ffill().iloc[:-1]
        new_ts._series *= factor
        new_ts._freq = freq
        if self.align == Alignments.RIGHT:
            new_ts = new_ts.to_right_align()
        return new_ts

    def to_unit(self, unit):
        pass  # TODO

    def sort(self, ascending=True):
        self._series = self._series.sort_index(ascending=ascending)

    def insert(self, timestamp, value):
        tmp_ts = self.create_from_tuples([(timestamp, value)], freq=self.freq, unit=self.unit, align=self.align,
                                         time_zone='infer')
        return self.append(tmp_ts, make_fit=False).sort()

    def append(self, other_ts, make_fit=False):
        if not make_fit:
            if self._unit != other_ts.unit:
                raise ValueError('units do not match')
            if not self._timezones_equal(self.time_zone, other_ts.time_zone):
                raise ValueError('time zones do not match')
            if self._align != other_ts.align:
                raise ValueError('alignments do not match')
            if self._freq != other_ts.freq:
                raise ValueError('frequencies do not match')
        else:
            pass  # TODO: try transform
        tmp_series = pd.concat([self._series, other_ts.as_pd_series()], verify_integrity=True, ignore_index=False)
        self._series = tmp_series
        return self

    def map(self, func):
        self._series = self._series.apply(func)
        return self

    def aggregate(self, func):
        return self._series.agg(func)

    def sum(self):
        return self.aggregate(np.sum)

    def get_gaps(self, start=None, end=None, freq=None, condensed=False):
        """
        Args:
            start (datetime.datetime): provide the start time of the search interval for gaps, if None the first
                timestamp in the series is considered as start, default: None
            end (datetime.datetime): provide the end time of the search interval for gaps, if None the last
                timestamp in the series is considered as end, default: None
            freq (Frequency): the search frequency for gaps, if None `self.freq` is used
            condensed (bool): combine consecutive gaps, if possible, default: False

        Returns:
            list of tuple: [(start (datetime.datetime), end (datetime.datetime), (...), ...]
                start and end have `self.timezone`
        """
        if not freq:
            if not self.freq:
                raise ValueError('cannot get gaps from timeseries with no freq')
            else:
                freq = self.freq
        _start = start or self.start_timestamp
        _end = end or self.end_timestamp
        tmp_series = self.to_left_align()
        expected_index = pd.date_range(
            _start, _end, freq=freq.pd_timedelta, tz=self.time_zone, closed='left')
        expected_series = pd.Series([np.NAN] * len(expected_index), index=expected_index)
        gap_series = expected_series.fillna(tmp_series._series)
        if condensed:
            consecutive_gap_starts = gap_series[
                (gap_series.isnull() & gap_series.shift(1, fill_value=1).notnull())].index.to_pydatetime().tolist()
            consecutive_gap_ends = gap_series[
                (gap_series.isnull() & gap_series.shift(-1, fill_value=1).notnull())
            ].index.shift(freq=freq.pd_timedelta).to_pydatetime().tolist()
            return list(zip(consecutive_gap_starts, consecutive_gap_ends))
        else:
            gap_index = gap_series[gap_series.isnull()].index
            starts = gap_index.to_pydatetime().tolist()
            ends = (gap_index + pd.Timedelta(freq.pd_timedelta)).to_pydatetime().tolist()
            return list(zip(starts, ends))

    def as_pd_series(self):
        return self._series

    def as_tuples(self):
        return list(zip(self._series.index, self._series))

    def as_dict(self, ordered=False):
        dict_class = dict if not ordered else collections.OrderedDict
        return self._series.to_dict(into=dict_class)

    def as_csv(self, file, header=True, sep=',', encoding='utf-8', decimal_sep='.', ):
        if header:
            h1 = 'timestamp' + (' [{tz}]'.format(tz=self.time_zone) if self.time_zone else '')
            h2 = 'value' + (' [{unit}]'.format(unit=self.unit) if self.unit else '')
            header = [h1, h2]
        self._series.to_csv(path_or_buf=file, sep=sep, header=header, index=True,
                            encoding=encoding, decimal=decimal_sep)

    def __getitem__(self, item):
        if isinstance(item, slice):
            new_ts = copy.deepcopy(self)
            new_ts._series = new_ts._series[item]
            return new_ts
        else:
            return self._series[item]

    def __setitem__(self, key, value):
        # ToDo: validate inputs
        self._series[key] = value
        return self

    def __len__(self):
        return self._series.size

    def __eq__(self, other):
        return self._series.equals(other._series) \
            and self.align == other.align \
            and self.unit == other.unit \
            and self._timezones_equal(self.time_zone, other.timezone) \
            and self.freq.pd_timedelta == other.freq.pd_timedela

    def __iter__(self):
        self._i = 0
        return self

    def __next__(self):
        if self._i < len(self):
            result = self[self._i]
            self._i += 1
            return result
        else:
            raise StopIteration

    def __add__(self, other):
        return self._basic_calc('__add__', other)

    def __sub__(self, other):
        return self._basic_calc('__sub__', other)

    def __mul__(self, other):
        return self._basic_calc('__mul__', other)

    def __div__(self, other):
        return self._basic_calc('__div__', other)

    def __truediv__(self, other):
        return self._basic_calc('__truediv__', other)

    def __floordiv__(self, other):
        return self._basic_calc('__floordiv__', other)

    def _basic_calc(self, operation, other):
        if isinstance(other, (collections.Sequence, np.ndarray, pd.Series)):
            if len(other) != len(self):
                raise ValueError("sequence has different length")
            if not all(map(lambda x: isinstance(x, numbers.Number), other)):
                raise ValueError("sequence contains non-numeric values")
        else:
            if not isinstance(other, numbers.Number):
                raise ValueError('value is not numeric')
        self._series = getattr(operation, self._series)(other)
        return self

    def __repr__(self):
        text = 'freq: {freq}, unit: {unit}, align: {align}, time zone: {time_zone}\n'.format(
            freq=self.freq,
            unit=self.unit,
            align=self.align,
            time_zone=self.time_zone
        )
        return text + str(self._series)

    def to_left_align(self):
        self._series = self._align.to_left(self._series, self._freq)
        self._align = Alignments.LEFT
        return self

    def to_right_align(self):
        self._series = self._align.to_right(self._series, self._freq)
        self._align = Alignments.RIGHT
        return self

    """ VALIDATORS FOR CONSTRUCTOR """

    def _validate_series(self, series):
        if not isinstance(series, pd.Series):
            raise ValueError('invalid series')
        if not isinstance(series.index, pd.DatetimeIndex):
            raise ValueError('index must be timestamps')
        if not pd.api.types.is_numeric_dtype(series.dtype):
            raise ValueError('values must be numeric')
        return series

    def _validate_time_zone(self, time_zone):
        if time_zone == 'infer':
            if self._series.empty:
                raise ValueError('cannot infer time zone from empty series')
            time_zone = self._infer_tz_from_timestamp(self._series.index[0])
        elif time_zone is None:
            if not self._series.empty:
                if self._infer_tz_from_timestamp(self._series.index[0]) is not None:
                    raise ValueError('no time zone passed, but timestamps are time zone aware')
        else:
            # validate time zone name (raises UnknownTimeZoneError if time_zone is invalid)
            pd.Timestamp('2000-01-01', tz=time_zone)
            # validate passed time zone against actual time zone
            if not self._series.empty:
                actual_tz = self._infer_tz_from_timestamp(self._series.index[0])
                if not self._timezones_equal(time_zone, actual_tz):
                    raise ValueError('passed time zone does not match actual time zone')
        return time_zone

    @staticmethod
    def _infer_tz_from_timestamp(timestamp):
        tzinfo = timestamp.tzinfo
        if tzinfo is None:
            return None
        if isinstance(tzinfo, dateutil.tz.tz.tzutc):
            return 'UTC'
        if isinstance(tzinfo, dateutil.tz.tz.tzlocal):
            # TODO: wait for https://github.com/dateutil/dateutil/issues/76 and make it nice
            for name in pytz.all_timezones:
                if name in tzinfo._tznames:
                    return name
                else:
                    raise TypeError(
                        'cannot infer time zone name from dateutil time zone info, consider using pytz instead')
        if isinstance(tzinfo, dateutil.tz.tz.tzfile):
            # TODO: wait for https://github.com/dateutil/dateutil/issues/76 and make it nice
            for name in pytz.all_timezones:
                if name in tzinfo._filename:
                    return name
                else:
                    raise TypeError(
                        'cannot infer time zone name from dateutil time zone info, consider using pytz instead')
        if isinstance(tzinfo, pytz.BaseTzInfo):
            return tzinfo.zone

    @staticmethod
    def _timezones_equal(tz1, tz2):
        if tz1 == None or tz2 == None:
            return tz1 == tz2
        else:
            today = dt.datetime.now()  # any timestamp
            return pd.Timestamp.today(tz=tz1).tz.utcoffset(today) == pd.Timestamp.today(tz=tz2).tz.utcoffset(today)

    def _validate_align(self, align):
        if isinstance(align, str):
            try:
                align = alignments[align]
            except KeyError:
                raise ValueError('invalid align')
        else:
            if not isinstance(align, Alignment):
                raise ValueError('invalid align')
        return align

    def _validate_unit(self, unit):
        if isinstance(unit, str):
            try:
                unit = units[unit]
            except KeyError:
                raise ValueError('invalid unit')
        elif unit is None:
            pass
        else:
            if not isinstance(unit, Unit):
                raise ValueError('invalid unit')
        return unit

    def _validate_freq(self, freq):
        if freq == 'infer':
            freq = Timeseries._infer_freq(self._series)
        elif freq is None:
            pass
        elif isinstance(freq, Frequency):
            if len(self._series) > 1:
                try:
                    inferred_freq = Timeseries._infer_freq(self._series)
                except ValueError:
                    raise ValueError('series has no constant freq')
                if freq.pd_timedelta != inferred_freq.pd_timedelta:
                    raise ValueError('passed freq does not match actual freq')
        else:
            raise ValueError('invalid freq')
        return freq

    @staticmethod
    def _infer_freq(series):
        if len(series) <= 1:
            raise ValueError("cannot infer freq from series with one or less elements")
        else:
            if series.index.freq is not None and hasattr(series.index.freq, 'delta'):
                freq = series.index.freq.delta
            else:
                freq = series.index[1] - series.index[0]
                diffs = series.index[1:] - series.index[:-1]
                if any([freq != f for f in diffs]):
                    raise ValueError('could not infer freq from series')
        try:
            freq = frequencies[freq]
        except KeyError:
            freq = CustomFrequency(freq)
        return freq
