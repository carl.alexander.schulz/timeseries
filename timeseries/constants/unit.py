class Unit:
    scale = None
    type = None

    def __str__(self):
        return '{scale}{type}'.format(scale=self.scale, type=self.type)

    def __repr__(self):
        return str(self)


class UnitType:
    symbol = None

    def __str__(self):
        return '{}'.format(self.symbol)


class Energy(UnitType):
    pass


class Power(UnitType):
    pass


class W(Power):
    symbol = 'W'


class Wh(Energy):
    symbol = 'Wh'


class UnitTypes:
    W = W()
    Wh = Wh()


class UnitScale:
    factor = None
    symbol = None

    def __str__(self):
        return '{}'.format(self.symbol)


class _NO_SCALE(UnitScale):
    factor = 1.
    symbol = ''


class _KILO(UnitScale):
    factor = 10.**3
    symbol = 'k'


class _MEGA(UnitScale):
    factor = 10.**6
    symbol = 'M'


class _GIGA(UnitScale):
    factor = 10.**9
    symbol = 'G'


class UnitScales:
    no = _NO_SCALE()
    k = _KILO()
    M = _MEGA()
    G = _GIGA()


class _Wh(Unit):
    scale = UnitScales.no
    type = UnitTypes.Wh


class _kWh(Unit):
    scale = UnitScales.k
    type = UnitTypes.Wh


class _MWh(Unit):
    scale = UnitScales.M
    type = UnitTypes.Wh


class _GWh(Unit):
    scale = UnitScales.G
    type = UnitTypes.Wh


class _W(Unit):
    scale = UnitScales.no
    type = UnitTypes.W


class _kW(Unit):
    scale = UnitScales.k
    type = UnitTypes.W


class _MW(Unit):
    scale = UnitScales.M
    type = UnitTypes.W


class _GW(Unit):
    scale = UnitScales.G
    type = UnitTypes.W


class Units:
    W = _W()
    kW = _kW()
    MW = _MW()
    GW = _GW()
    Wh = _Wh()
    kWh = _kWh()
    MWh = _MWh()
    GWh = _GWh()


units = {
    'W': _W(),
    'kW': _kW(),
    'MW': _MW(),
    'GW': _GW(),
    'Wh': _Wh(),
    'kWh': _kWh(),
    'MWh': _MWh(),
    'GWh': _GWh(),
}

