class Alignment:

    def to_left(self, series, freq):
        return series

    def to_right(self, series, freq):
        return series

    def __repr__(self):
        return str(self)


class LEFT(Alignment):

    def to_right(self, series, freq):
        series.index += freq.pd_timedelta
        return series

    def __str__(self):
        return 'left'


class RIGHT(Alignment):

    def to_left(self, series, freq):
        series.index -= freq.pd_timedelta
        return series

    def __str__(self):
        return 'right'


class Alignments:
    LEFT = LEFT()
    RIGHT = RIGHT()


alignments = {'left': LEFT(),
              'right': RIGHT()}
