import pandas as pd


class Frequency:
    pd_timedelta = None

    @property
    def timedelta(self):
        return self.pd_timedelta.to_pytimedelta()

    def __str__(self):
        return str(self.timedelta)

    def __repr__(self):
        return str(self)

    def __eq__(self, other):
        return self.pd_timedelta == other.pd_timedelta


class CustomFrequency(Frequency):
    def __init__(self, pd_timedelta):
        self.pd_timedelta = pd_timedelta


class _5_MINUTES(Frequency):
    pd_timedelta = pd.Timedelta(minutes=5)


class _15_MINUTES(Frequency):
    pd_timedelta = pd.Timedelta(minutes=15)


class _1_HOUR(Frequency):
    pd_timedelta = pd.Timedelta(hours=1)


class _1_DAY(Frequency):
    pd_timedelta = pd.Timedelta(days=1)


class _1_WEEK(Frequency):
    pd_timedelta = pd.Timedelta(weeks=1)


class _1_MONTH(Frequency):
    pd_timedelta = pd.Timedelta('1M')


class _1_YEAR(Frequency):
    pd_timedelta = pd.Timedelta('1Y')


class Frequencies:
    FIVE_MINUTES = _5_MINUTES()
    FIFTEEN_MINUTES = _15_MINUTES()
    HOUR = _1_HOUR()
    DAY = _1_DAY()
    WEEK = _1_WEEK()
    MONTH = _1_MONTH()
    YEAR = _1_YEAR()


frequencies = {
    pd.Timedelta(minutes=5): _5_MINUTES(),
    pd.Timedelta(minutes=15): _15_MINUTES(),
    pd.Timedelta(hours=1): _1_HOUR(),
    pd.Timedelta(days=1): _1_DAY(),
    pd.Timedelta(weeks=1): _1_WEEK(),
    pd.Timedelta('1M'): _1_MONTH(),
    pd.Timedelta('1Y'): _1_YEAR(),
}